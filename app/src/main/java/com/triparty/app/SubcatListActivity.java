package com.triparty.app;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.applidium.shutterbug.FetchableImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class SubcatListActivity extends Activity {

	ListView listView;
	Custom_Adapter adapter;
	String subcategory;
	String subcategoryid;
	private ArrayList<SubCategoryList> subcategorylist = new ArrayList<SubCategoryList>();
	
	JSONArray json1;
	SharedPreferences prefs;
	private AdView adView;
	Typeface normal, bold;
	private static final String AD_UNIT_ID = DataManager.admobid;
	boolean cbonline;
	TextView txtheader;
	private ProgressDialog progress;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subcat_list);



		normal = Typeface.createFromAsset(getAssets(), "normal.ttf");
		bold = Typeface.createFromAsset(getAssets(), "bold.ttf");
		adView = new AdView(this);
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		subcategorylist = DataManager.subcategorylist;

		adapter = new Custom_Adapter(SubcatListActivity.this);
		
		
		AdRequest adRequest = new AdRequest.Builder().build();
		txtheader = (TextView) findViewById(R.id.txtheader);
		txtheader.setTypeface(normal);
		adView.loadAd(adRequest);
		LinearLayout ll = (LinearLayout) findViewById(R.id.ad);
		ll.addView(adView);
		Button home = (Button) findViewById(R.id.btnback);

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(SubcatListActivity.this,
						MainActivity.class);
				finish();
				startActivity(i);

			}
		});

		listView = (ListView) findViewById(R.id.lvsub__cat);

		

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				subcategoryid = subcategorylist.get(position).getId();
				
				new getquestionsbysubcat().execute();
			}

		});
		
		displaydata();
	}

	public void displaydata() {

		adapter.notifyDataSetChanged();
		listView.setAdapter(adapter);

	}

	public class Custom_Adapter extends BaseAdapter {

		private LayoutInflater mInflater;

		public Custom_Adapter(Context c) {
			mInflater = LayoutInflater.from(c);

		}

		@Override
		public int getCount() {
			return subcategorylist.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {

				convertView = mInflater.inflate(R.layout.category_row, null);

				holder = new ViewHolder();

				holder.txtcatname = (TextView) convertView
						.findViewById(R.id.txtcustomrow);

				holder.img = (FetchableImageView) convertView
						.findViewById(R.id.img);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.txtcatname.setText(subcategorylist.get(position)
					.getSubcatname());

			String url = DataManager.photourl
					+ subcategorylist.get(position).getSub_category_image();
			try {

				url = URLDecoder.decode(url, "UTF-8");
				url = url.replaceAll(" ", "%20");
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			holder.img.setImage(url,
					getResources().getDrawable(R.drawable.splash_icon));

			return convertView;
		}

		class ViewHolder {
			TextView txtcatname;
			FetchableImageView img;

		}

	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onStop() {
		super.onStop();

	}

	@Override
	public void onBackPressed() {

		Intent i = new Intent(SubcatListActivity.this, MainActivity.class);
		finish();
		startActivity(i);

	}
	
	 public class getquestionsbysubcat extends AsyncTask<String, Void, String> {
			boolean response = false;

			@Override
			protected void onPreExecute() {

				progress = ProgressDialog.show(SubcatListActivity.this, "Getting Data...",
						"Please wait....");
			}

			@Override
			protected String doInBackground(String... params) {

				response = APIManager.getquestionbysubcat(subcategoryid);

				return "";

			}

			@Override
			protected void onPostExecute(String result) {

				progress.cancel();
				if (response) {
					
					if (DataManager.status.equalsIgnoreCase("1")) {

						Intent i = new Intent(SubcatListActivity.this, Timer_questions.class);
						startActivity(i);

						
					} else {
							
						connectionerror();
					
					}
				} else {
					
					connectionerror();
				}

			}

			@Override
			protected void onProgressUpdate(Void... values) {

			}
		}
	 
	  public void connectionerror() {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubcatListActivity.this);

			alertDialog.setTitle("Error!");

			alertDialog.setMessage("Connection Lost ! Try Again");

			alertDialog.setPositiveButton("Retry",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							new getquestionsbysubcat().execute();

						}
					});

			alertDialog.show();
		}
}