package com.triparty.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.applidium.shutterbug.FetchableImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Timer_questions extends Activity implements
		TextToSpeech.OnInitListener, View.OnClickListener {
	String que, opt1, opt2, opt3, opt4, answer, category, url;
	int rightans = 0;
	int wrongans = 0;
	int i = 0;
	String right, wrong, next;
	private TextToSpeech mText2Speech;
	ArrayList<QuestionsPojo> questionlist = new ArrayList<QuestionsPojo>();
	TextView taOpt1, taOpt2, taOpt3, taOpt4, txtquesnum, tv;

	TextView taQue;
	static int currentQuestion = 0;
	ToggleButton tgbtn;
	MediaPlayer mp;
	QuizPojo cn = null;
	MyCounter timer = null;
	Setting_preference pref;
	String no_of_questions, strtimer;
	long savedtimer;
	int totalQueLen;
	boolean cbsound, cbvibrate, cbtimer;
	Vibrator vibe;
	String categoryname;
	private AdView adView;
	Button btnfifty, btnpass, btntimer;
	private static final String AD_UNIT_ID = DataManager.admobid;
	TextView[] btn1, btn2, btn3, btn4;
	Random rand = new Random();
	boolean fifty = false, pass = false, time = false;
	Typeface normal, bold;
	LinearLayout lltimer;
//	int fiftfifty = 0, skip = 0, timelifeline = 0;
	FetchableImageView img;
	int addcounter = 0;
	SharedPreferences myPrefs;
	SharedPreferences.Editor prefsEditor;
	private final String TAG_NAME = "ads";
	private InterstitialAd interstitial;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quizscreen);

		myPrefs = this.getSharedPreferences("myPrefs", MODE_WORLD_READABLE);
		prefsEditor = myPrefs.edit();
		addcounter = myPrefs.getInt(TAG_NAME, 0);

		questionlist = DataManager.questionlist;

		normal = Typeface.createFromAsset(getAssets(), "normal.ttf");
		bold = Typeface.createFromAsset(getAssets(), "bold.ttf");

		tv = (TextView) findViewById(R.id.tv);
		tv.setText(" ");

		pref = new Setting_preference(getApplicationContext());

		strtimer = DataManager.timer;
		vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		savedtimer = Long.parseLong(strtimer);
		categoryname = getIntent().getStringExtra("categoryname");

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(Timer_questions.this);

		tgbtn = (ToggleButton) this.findViewById(R.id.toggleButton2);
		cbsound = prefs.getBoolean("cbsound", true);
		cbvibrate = prefs.getBoolean("cbvibrate", true);
		cbtimer = prefs.getBoolean("cbtimer", true);
		btntimer = (Button) this.findViewById(R.id.btntimer);
		lltimer = (LinearLayout) this.findViewById(R.id.lltimer);
		tgbtn.setText(null);
		tgbtn.setTextOn(null);
		tgbtn.setTextOff(null);
		if (cbsound) {
			tgbtn.setChecked(true);
		}

		if (cbtimer) {
			timer = new MyCounter(savedtimer * 1000, 1000);
			timer.start();
		} else {
			tv.setVisibility(View.INVISIBLE);
			btntimer.setVisibility(View.GONE);
			lltimer.setVisibility(View.GONE);
		}
		mText2Speech = new TextToSpeech(Timer_questions.this,
				Timer_questions.this);

		currentQuestion = 0;
		rightans = 0;
		wrongans = 0;
		taQue = (TextView) this.findViewById(R.id.taque);
		taOpt1 = (TextView) this.findViewById(R.id.taOpt5);
		taOpt2 = (TextView) this.findViewById(R.id.taOpt6);
		taOpt3 = (TextView) this.findViewById(R.id.taOpt7);
		taOpt4 = (TextView) this.findViewById(R.id.taOpt8);
		img = (FetchableImageView) findViewById(R.id.imageView1);

		btnfifty = (Button) this.findViewById(R.id.btnfifty);
		btnpass = (Button) this.findViewById(R.id.btnskip);

		txtquesnum = (TextView) this.findViewById(R.id.txtquesnum);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		right = "Right answer";
		wrong = "Wrong answer";
		next = "Next Question";
		taOpt1.setOnClickListener(this);
		taOpt2.setOnClickListener(this);
		taOpt3.setOnClickListener(this);
		taOpt4.setOnClickListener(this);

		taOpt1.setTypeface(bold);
		taOpt2.setTypeface(bold);
		taOpt3.setTypeface(bold);
		taOpt4.setTypeface(bold);

		btnfifty.setTypeface(bold);
		btntimer.setTypeface(bold);
		btnpass.setTypeface(bold);
		taQue.setTypeface(bold);
		txtquesnum.setTypeface(normal);
		tv.setTypeface(bold);
		totalQueLen = questionlist.size();

		getquestionsanswers(currentQuestion);

		TextView[] arr1 = { taOpt2, taOpt3, taOpt4 };
		TextView[] arr2 = { taOpt1, taOpt3, taOpt4 };
		TextView[] arr3 = { taOpt1, taOpt2, taOpt4 };
		TextView[] arr4 = { taOpt2, taOpt3, taOpt1 };
		btn1 = arr1;
		btn2 = arr2;
		btn3 = arr3;
		btn4 = arr4;
//		fiftfifty = pref.getfifty();
//		skip = pref.getSkip();
//		timelifeline = pref.gettimer();
//
//		btnfifty.setText("50-50 : " + fiftfifty);
//		btnpass.setText("SKIP : " + skip);
//		btntimer.setText("TIMER : " + timelifeline);

		btnfifty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			

					System.out.println("answer--" + answer);
					fifty = true;
					taOpt1.setVisibility(View.INVISIBLE);
					taOpt2.setVisibility(View.INVISIBLE);
					taOpt3.setVisibility(View.INVISIBLE);
					taOpt4.setVisibility(View.INVISIBLE);
					if (answer.equalsIgnoreCase(taOpt1.getText().toString())) {
						taOpt1.setVisibility(View.VISIBLE);
						TextView id = btn1[rand.nextInt(btn1.length)];
						id.setVisibility(View.VISIBLE);
					} else if (answer.equalsIgnoreCase(taOpt2.getText()
							.toString())) {
						taOpt2.setVisibility(View.VISIBLE);
						TextView id = btn2[rand.nextInt(btn2.length)];
						id.setVisibility(View.VISIBLE);
					} else if (answer.equalsIgnoreCase(taOpt3.getText()
							.toString())) {
						taOpt3.setVisibility(View.VISIBLE);
						TextView id = btn3[rand.nextInt(btn4.length)];
						id.setVisibility(View.VISIBLE);
					} else if (answer.equalsIgnoreCase(taOpt4.getText()
							.toString())) {
						taOpt4.setVisibility(View.VISIBLE);
						TextView id = btn4[rand.nextInt(btn4.length)];
						id.setVisibility(View.VISIBLE);
					}

					
				btnfifty.setVisibility(View.GONE);

			}
		});
		btntimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			
					if (cbtimer) {
						time = true;
						long newtime = 16000;
						timer.cancel();
						timer = new MyCounter(newtime, 1000);
						timer.start();
						// btntimer.setVisibility(View.INVISIBLE);
						
						btntimer.setVisibility(View.GONE);
					
				}
			}
		});

		btnpass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				
					pass = true;
					nextquestion(0);
					btnpass.setVisibility(View.GONE);
			}
		});

		adView = new AdView(this);

		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(AD_UNIT_ID);
		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);
		LinearLayout ll = (LinearLayout) findViewById(R.id.ad);
		ll.addView(adView);

		interstitial = new InterstitialAd(this);
		interstitial.setAdUnitId("ca-app-pub-6192865524332826/7070374392");

		// Create ad request.
		AdRequest adRequest1 = new AdRequest.Builder().build();

		// Begin loading your interstitial.
		interstitial.loadAd(adRequest1);
	}

	public void updaterateCounter() {
		addcounter++;
		prefsEditor.putInt(TAG_NAME, addcounter);
		prefsEditor.commit();

		if (addcounter == DataManager.addcounter) {
			displayInterstitial();
			addcounter = 0;
			prefsEditor.putInt(TAG_NAME, addcounter);
			prefsEditor.commit();
		}

	}

	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}

	}

	@Override
	public void onClick(View v1) {

		if (cbtimer) {
			timer.cancel();
		}
		TextView tmp = (TextView) v1;
		String sel = tmp.getText().toString();

		selectanswer(0, tmp, sel);
	}

	
	public void selectanswer(int SPLASHTIME, final TextView tmp,
			final String sel) {

		if (sel.equals(answer)) {

			tmp.setBackgroundResource(R.drawable.green);
			tmp.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.right_icon, 0);
			vibrate();
			if (tgbtn.isChecked()) {
				audio(right);
			}
			rightans++;
			nextquestion(3000);
		} else {

			tmp.setBackgroundResource(R.drawable.red);
			tmp.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.wrong_icon, 0);

			if (answer.equals(taOpt1.getText().toString())) {
				taOpt1.setBackgroundResource(R.drawable.green);
				taOpt1.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.right_icon, 0);

			}
			if (answer.equals(taOpt2.getText().toString())) {
				taOpt2.setBackgroundResource(R.drawable.green);
				taOpt2.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.right_icon, 0);

			}
			if (answer.equals(taOpt3.getText().toString())) {
				taOpt3.setBackgroundResource(R.drawable.green);
				taOpt3.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.right_icon, 0);

			}
			if (answer.equals(taOpt4.getText().toString())) {
				taOpt4.setBackgroundResource(R.drawable.green);
				taOpt4.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.right_icon, 0);

			}
			wrongans++;
			if (tgbtn.isChecked()) {

				audio(wrong);
			}

			vibrate();

			nextquestion(4000);
		}

		taOpt1.setClickable(false);
		taOpt2.setClickable(false);
		taOpt3.setClickable(false);
		taOpt4.setClickable(false);

	}

	public void nextquestion(int SPLASHTIME) {

		if (cbtimer) {
			timer.cancel();
		}
		Handler handler = new Handler();

		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				updaterateCounter();
				currentQuestion++;

				if (currentQuestion < totalQueLen) {
					if (cbtimer) {
						timer = new MyCounter(savedtimer * 1000, 1000);
						timer.start();
					}
					getquestionsanswers(currentQuestion);
					if (tgbtn.isChecked()) {
						audio(next);
					}

				} else {

					if (cbtimer) {
						timer.cancel();
					}
					Intent iScore = new Intent(Timer_questions.this,
							Score.class);
					iScore.putExtra("rightans", rightans);
					iScore.putExtra("totalques", totalQueLen);
					iScore.putExtra("category", category);
					finish();
					startActivity(iScore);

				}

			}

		}, SPLASHTIME);
	}

	@Override
	public void onInit(int status) {

		if (status == TextToSpeech.SUCCESS) {
			mText2Speech.setLanguage(Locale.getDefault());
		}

	}

	@SuppressWarnings("deprecation")
	public void audio(String message) {
		if (cbsound) {
			mText2Speech.speak(message, TextToSpeech.QUEUE_FLUSH, null);
		}
	}

	public void vibrate() {
		if (cbvibrate) {
			vibe.vibrate(700);
		}
	}

	public void getquestionsanswers(int index) {

		que = questionlist.get(index).getQuestion();
		opt1 = questionlist.get(index).getOpt1();
		opt2 = questionlist.get(index).getOpt2();
		opt3 = questionlist.get(index).getOpt3();
		opt4 = questionlist.get(index).getOpt4();
		answer = questionlist.get(index).getAnswer();
		url = questionlist.get(index).getImage();

		setCurrentQuestion();

	}

	public void setCurrentQuestion() {
		updaterateCounter();
		try {

			i++;
			txtquesnum.setText("" + i + " / " + totalQueLen);
			taOpt1.setVisibility(View.VISIBLE);
			taOpt2.setVisibility(View.VISIBLE);
			taOpt3.setVisibility(View.VISIBLE);
			taOpt4.setVisibility(View.VISIBLE);

			taOpt1.setClickable(true);
			taOpt2.setClickable(true);
			taOpt3.setClickable(true);
			taOpt4.setClickable(true);

			ArrayList<String> optionlist = new ArrayList<String>();

			optionlist.add(opt1);
			optionlist.add(opt2);
			optionlist.add(opt3);
			optionlist.add(opt4);

			Collections.shuffle(optionlist);
			taOpt1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			taOpt2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			taOpt3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			taOpt4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			taOpt1.setTextColor(Color.BLACK);
			taOpt2.setTextColor(Color.BLACK);
			taOpt3.setTextColor(Color.BLACK);
			taOpt4.setTextColor(Color.BLACK);
			taOpt1.setBackgroundResource(R.drawable.normal);
			taOpt2.setBackgroundResource(R.drawable.normal);
			taOpt3.setBackgroundResource(R.drawable.normal);
			taOpt4.setBackgroundResource(R.drawable.normal);
			taQue.setText(que);
			taOpt1.setText(optionlist.get(0).toString());
			taOpt2.setText(optionlist.get(1).toString());
			taOpt3.setText(optionlist.get(2).toString());
			taOpt4.setText(optionlist.get(3).toString());

			if (url.equalsIgnoreCase("blank")) {
				img.setVisibility(View.GONE);
			} else {
				url = DataManager.photourl + url;
				img.setVisibility(View.VISIBLE);
				img.setImage(url);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class MyCounter extends CountDownTimer {

		public MyCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {

			nextquestion(1000);

		}

		@Override
		public void onTick(long millisUntilFinished) {

			Integer milisec = new Integer(
					new Double(millisUntilFinished).intValue());
			Integer cd_secs = milisec / 1000;

			Integer seconds = (cd_secs % 3600) % 60;

			tv.setText("Timer  : " + String.format("%02d", seconds));
		}

	}

	@Override
	public void onBackPressed() {
		if (cbtimer) {
			timer.cancel();
		}
		new AlertDialog.Builder(this)
				.setTitle("Really Exit?")
				.setMessage("Do you want to leave this Test?")
				.setNegativeButton(android.R.string.no, null)
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface arg0, int arg1) {

								if (cbtimer) {
									timer.cancel();
								}

								Intent i = new Intent(Timer_questions.this,
										Score.class);
								i.putExtra("rightans", rightans);
								i.putExtra("totalques", totalQueLen);
								i.putExtra("category", category);
								finish();
								startActivity(i);
							}
						}).create().show();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (cbtimer) {
			timer.cancel();

		}
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (cbtimer) {
			timer.cancel();
		}
	}

}
