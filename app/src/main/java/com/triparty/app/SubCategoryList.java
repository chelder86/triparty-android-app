package com.triparty.app;

public class SubCategoryList {
	
		String id, subcatname, sub_category_image;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSubcatname() {
			return subcatname;
		}

		public void setSubcatname(String subcatname) {
			this.subcatname = subcatname;
		}

		public String getSub_category_image() {
			return sub_category_image;
		}

		public void setSub_category_image(String sub_category_image) {
			this.sub_category_image = sub_category_image;
		}

	

}
