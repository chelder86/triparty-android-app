package com.triparty.app;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;



public class APIManager {
	
	
	public static String url = DataManager.url;
	
	
	public static boolean getcategories() {
		boolean result = false;

		
		String geturl = url + "getCategories.php";
		HttpParams param = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(param, 4000);
		HttpConnectionParams.setSoTimeout(param, 10000);

		HttpClient client = new DefaultHttpClient(param);
		// Send Httpget request
		HttpGet get = new HttpGet(geturl);
		

		
		try {

		
			HttpResponse responsePOST;
			String response = null;

			responsePOST = client.execute(get);

			HttpEntity resEntity = responsePOST.getEntity();

			if (resEntity != null) {

				// get response
				response = EntityUtils.toString(resEntity);
				result = true;
				
				ArrayList<CategoryList> categorylist = new ArrayList<CategoryList>();
				
				try {

					JSONObject c = new JSONObject(response);
					
					String status = c.getString("success");
					
					if(status.equals("1"))
					{
						JSONArray jarray = c.getJSONArray("category");
						
						System.out.println("response---"+response);
						
						
						for(int i =0; i < jarray.length(); i++)
							
						{
							CategoryList cat = new CategoryList();
							
							
							
							String id =  new String(jarray.getJSONObject(i)
									.getString("id").getBytes("UTF-8"),
									"UTF-8");
							String category_name =  new String(jarray.getJSONObject(i)
									.getString("category_name").getBytes("UTF-8"),
									"UTF-8");
							String category_image =  new String(jarray.getJSONObject(i)
									.getString("category_image").getBytes("UTF-8"),
									"UTF-8");
							String subcat_count =  new String(jarray.getJSONObject(i)
									.getString("subcat_count").getBytes("UTF-8"),
									"UTF-8");
						
							
							cat.setId(id);
							cat.setName(category_name);
							cat.setSubcat_count(subcat_count);
							cat.setCategory_image(category_image);
							
							
							categorylist.add(cat);
						}
						
						DataManager.categorylist = categorylist;
						
						DataManager.status = status;
					}else
					{
						DataManager.status = status;
					}
					
					

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}

			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	
	public static boolean getsubcategories(String categoryid) {
		boolean result = false;

		
		String geturl = url + "getSubCategory.php?category_id="+categoryid;
		
		System.out.println("geturl---"+geturl);
		HttpParams param = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(param, 4000);
		HttpConnectionParams.setSoTimeout(param, 10000);

		HttpClient client = new DefaultHttpClient(param);
		// Send Httpget request
		HttpGet get = new HttpGet(geturl);
		

		
		try {

		
			HttpResponse responsePOST;
			String response = null;

			responsePOST = client.execute(get);

			HttpEntity resEntity = responsePOST.getEntity();

			if (resEntity != null) {

				// get response
				response = EntityUtils.toString(resEntity);
				result = true;
				
				 ArrayList<SubCategoryList> subcategorylist = new ArrayList<SubCategoryList>();
				System.out.println("response---"+response);
				try {

					JSONObject c = new JSONObject(response);
					
					String status = c.getString("success");
					
					if(status.equals("1"))
					{
						JSONArray jarray = c.getJSONArray("sub_category");
						
					
						
						for(int i =0; i < jarray.length(); i++)
							
						{
							SubCategoryList subcat = new SubCategoryList();
							

							String id =  new String(jarray.getJSONObject(i)
									.getString("id").getBytes("UTF-8"),
									"UTF-8");
							

							String sub_category_name =  new String(jarray.getJSONObject(i)
									.getString("sub_category_name").getBytes("UTF-8"),
									"UTF-8");

							String sub_category_image =  new String(jarray.getJSONObject(i)
									.getString("sub_category_image").getBytes("UTF-8"),
									"UTF-8");
							
							
						
							
							subcat.setId(id);
							subcat.setSubcatname(sub_category_name);
							subcat.setSub_category_image(sub_category_image);
							
							
							
							subcategorylist.add(subcat);
						}
						
						DataManager.subcategorylist = subcategorylist;
						
						DataManager.status = status;
					}else
					{
						DataManager.status = status;
					}
					
					

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}

			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	
	public static boolean getquestionbysubcat(String subcatid) {
		boolean result = false;

		
		String geturl = url + "getQuestionBySubCatId.php?subcategory_id="+subcatid;
		HttpParams param = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(param, 4000);
		HttpConnectionParams.setSoTimeout(param, 10000);

		HttpClient client = new DefaultHttpClient(param);
		// Send Httpget request
		HttpGet get = new HttpGet(geturl);
		

		
		try {

		
			HttpResponse responsePOST;
			String response = null;

			responsePOST = client.execute(get);

			HttpEntity resEntity = responsePOST.getEntity();

			if (resEntity != null) {

				// get response
				response = EntityUtils.toString(resEntity);
				result = true;
				
				 ArrayList<QuestionsPojo> questionlist = new ArrayList<QuestionsPojo>();
					System.out.println("response---"+response);
					try {

						JSONObject c = new JSONObject(response);
						
						String status = c.getString("success");
						
						if(status.equals("1"))
						{
							JSONArray jarray = c.getJSONArray("questions");
							
							JSONObject jobj = null;
							
							for(int i =0; i < jarray.length(); i++)
								
							{
								QuestionsPojo que = new QuestionsPojo();
								
								jobj = jarray.getJSONObject(i);
								
								String id = jobj.getString("id");
								String question =  jobj.getString("question_text");
								String quesimage = jobj.getString("quesimage");
								String option1 = jobj.getString("option1");
								String option2 = jobj.getString("option2");
								String option3 = jobj.getString("option3");
								String option4 = jobj.getString("option4");
								String correctans = jobj.getString("correctans");
								
								if(correctans.equalsIgnoreCase("option1"))
								{
									correctans = option1;
								}else if(correctans.equalsIgnoreCase("option2"))
								{
									correctans = option2;
								}else if(correctans.equalsIgnoreCase("option3"))
								{
									correctans = option3;
								}else if(correctans.equalsIgnoreCase("option4"))
								{
									correctans = option4;
								}
							
								
								
								que.setQid(id);
								que.setQuestion(question);
								que.setImage(quesimage);
								que.setOpt1(option1);
								que.setOpt2(option2);
								que.setOpt3(option3);
								que.setOpt4(option4);
								que.setAnswer(correctans);
								
								
								questionlist.add(que);
							}
							
							DataManager.questionlist = questionlist;
							
							DataManager.status = status;
						}else
						{
							DataManager.status = status;
						}
						
					

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}

			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	
	public static boolean getquestionbycatagory(String categoryid) {
		boolean result = false;

		
		String geturl = url + "getQuestionByCatId.php?category_id="+categoryid;
		HttpParams param = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(param, 4000);
		HttpConnectionParams.setSoTimeout(param, 10000);

		HttpClient client = new DefaultHttpClient(param);
		// Send Httpget request
		HttpGet get = new HttpGet(geturl);
		

		
		try {

		
			HttpResponse responsePOST;
			String response = null;

			responsePOST = client.execute(get);

			HttpEntity resEntity = responsePOST.getEntity();

			if (resEntity != null) {

				// get response
				response = EntityUtils.toString(resEntity);
				result = true;
				
				 ArrayList<QuestionsPojo> questionlist = new ArrayList<QuestionsPojo>();
				System.out.println("response---"+response);
				try {

					JSONObject c = new JSONObject(response);
					
					String status = c.getString("success");
					
					if(status.equals("1"))
					{
						JSONArray jarray = c.getJSONArray("questions");
						
						JSONObject jobj = null;
						
						for(int i =0; i < jarray.length(); i++)
							
						{
							QuestionsPojo que = new QuestionsPojo();
							
							jobj = jarray.getJSONObject(i);
							
							String id = jobj.getString("id");
							String question =  jobj.getString("question_text");
							String quesimage = jobj.getString("quesimage");
							String option1 = jobj.getString("option1");
							String option2 = jobj.getString("option2");
							String option3 = jobj.getString("option3");
							String option4 = jobj.getString("option4");
							String correctans = jobj.getString("correctans");
							
							if(correctans.equalsIgnoreCase("option1"))
							{
								correctans = option1;
							}else if(correctans.equalsIgnoreCase("option2"))
							{
								correctans = option2;
							}else if(correctans.equalsIgnoreCase("option3"))
							{
								correctans = option3;
							}else if(correctans.equalsIgnoreCase("option4"))
							{
								correctans = option4;
							}
						
							
							
							que.setQid(id);
							que.setQuestion(question);
							que.setImage(quesimage);
							que.setOpt1(option1);
							que.setOpt2(option2);
							que.setOpt3(option3);
							que.setOpt4(option4);
							que.setAnswer(correctans);
							
							
							questionlist.add(que);
						}
						
						DataManager.questionlist = questionlist;
						
						DataManager.status = status;
					}else
					{
						DataManager.status = status;
					}
					
					

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}

			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}


}
