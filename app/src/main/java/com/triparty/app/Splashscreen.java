package com.triparty.app;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;


public class Splashscreen extends Activity {


	Setting_preference pref;
	private boolean mIsBackButtonPressed;
	private static final int SPLASH_DURATION = 3000; // 3 seconds


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splashscreen);

	
		pref = new Setting_preference(this);

		
		Handler handler = new Handler();

		// run a thread after 2 seconds to start the home screen
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {

				if (!mIsBackButtonPressed) {

					pref.editor.putBoolean("sound", true);
					pref.editor.putBoolean("vibrate", true);

					
					Intent i = new Intent(Splashscreen.this, MainActivity.class);
					finish();
					startActivity(i);
					
					
				}
			}

		}, SPLASH_DURATION);

	}
	
	

	@Override
	public void onBackPressed() {

		// set the flag to true so the next activity won't start up
		mIsBackButtonPressed = true;
		super.onBackPressed();

	}

}